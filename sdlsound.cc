/*
  Copyright (C) 2000-2016  Hideki EIRAKU

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <cmath>
#include <fstream>
#include <cstdio>
#include <iostream>

#include "SDL.h"
#include "jmem.hh"
#include "jvideo.hh"
#include "jsound.hh"
#include "sdlsound.hh"
#include "8259a.hh"

using std::cout;
using std::cerr;
using std::endl;

void
sdlsound::audiocallback (Uint8 *stream, int len)
{
  int size = SDL_AtomicGet (&fillsize);
  if (!playing && size >= samples * 2)
    playing = true;
  if (playing)
    {
      const int bufsize = buffersize;
      short *buf = (short *)stream;
      len /= 2;
      while (len > 0 && size > 0)
	{
	  const unsigned int off = copyoffset;
	  if (size > len)
	    size = len;
	  for (int i = 0; i < size; i++)
	    buf[i] = localbuf[(off + i) % bufsize];
	  const unsigned int newoff = (off + size) % bufsize;
	  if (off + size != newoff)
	    videohw.sync_audio (clkcount_at_buf0);
	  copyoffset = newoff;
	  buf += size;
	  len -= size;
	  int oldsize = SDL_AtomicAdd (&fillsize, -size);
	  if (oldsize == bufsize)
	    SDL_SemPost (semaphore);
	  size = oldsize - size;
	}
      if (len > 0)
	{
	  cerr << "Audio buffer underflow" << "\tbuffersize " << buffersize
	       << " len " << len << endl;
	  for (int i = 0; i < len; i++)
	    buf[i] = 0;
	  playing = false;
	}
    }
}

void
sdlsound::sdlaudiocallback (void *data, Uint8 *stream, int len)
{
  sdlsound *p = static_cast<sdlsound *> (data);

  p->audiocallback (stream, len);
}

Uint32
sdlsound::sdltimercallback (Uint32 interval, void *param)
{
  sdlsound *p = static_cast<sdlsound *> (param);
  Uint8 buf[p->samples * 2];

  p->audiocallback (buf, sizeof buf);
  if (p->closing)
    {
      SDL_SemPost (p->closing);
      return 0;
    }
  return interval;
}

sdlsound::sdlsound (unsigned int rate, unsigned int buffersize,
		    unsigned int samples, jvideo::hw &videohw)
  : samples (samples), videohw (videohw)
{
  unsigned int i;
  SDL_AudioSpec fmt;

  // Gate 0 and 1 of PIT are always high
  pit.setgate (0, true);
  pit.setgate (1, true);

  // Initialize CLK counter
  timerclk = 0;
  soundclk = 0;
  clksum = 0;
  clkcount = 0;

  // Initialize audio buffer
  sdlsound::rate = rate;
  sdlsound::buffersize = buffersize;
  localbuf = new signed short [buffersize];
  for (i = 0; i < buffersize; i++)
    localbuf[i] = 0;

  // Initialize variables used by callback handler
  copyoffset = 0;
  filloffset = 0;
  SDL_AtomicSet (&fillsize, 0);
  playing = false;
  semaphore = SDL_CreateSemaphore (0);
  clkcount_at_buf0 = 0;

  // Open SDL audio
  timer_id = 0;
  fmt.freq = rate;
  fmt.format = AUDIO_S16SYS;
  fmt.channels = 1;
  fmt.samples = samples;
  fmt.callback = sdlaudiocallback;
  fmt.userdata = (void *)this;
  if (SDL_OpenAudio (&fmt, NULL) < 0)
    {
      cerr << "SDL_OpenAudio failed. Continue without audio." << endl;
      sdlsound::rate = 10000;
      samples = 100;
      closing = NULL;
      timer_id = SDL_AddTimer (10, sdltimercallback, this);
      if (!timer_id)
	cerr << "SDL_AddTimer failed." << endl;
      return;
    }
  SDL_PauseAudio (0);
}

sdlsound::~sdlsound ()
{
  if (timer_id)
    {
      closing = SDL_CreateSemaphore (0);
      SDL_SemWait (closing);
      SDL_DestroySemaphore (closing);
    }
  else
    SDL_CloseAudio ();
  SDL_DestroySemaphore (semaphore);
  delete [] localbuf;
}

void
sdlsound::tick_pit ()
{
  bool out00 = pit.getout (0);
  pit.tick (0);
  bool out01 = pit.getout (0);
  if (!out00 && out01)
    trigger_irq8259 (0);
  else if (out00 && !out01)
    untrigger_irq8259 (0);
  if (!timer1sel || (!out00 && out01))
    pit.tick (1);
  pit.tick (2);
  if ((pb & 0x2) == 0 || !pit.getout (2)) // !PB1 || !Timer2output
    {
      outbeep0++;
      internalbeep0++;
    }
  else
    {
      outbeep1++;
      if ((pb & 0x10) != 0) // PB4
	internalbeep0++;
      else
	internalbeep1++;
    }
}

void
sdlsound::tick_sound ()
{
  sn76489a.tick ();
}

void
sdlsound::tick_genaudio ()
{
  int soundtmp = sn76489a.getdata ();
  int speakerin = 8192 * internalbeep1 / (internalbeep0 + internalbeep1);
  int speakerout;
  switch (pb & 0x60)	// PB5 and PB6
    {
    default:		// Make compiler happy
    case 0x00:		// Timer 2 output
      speakerout = 8192 * outbeep1 / (outbeep0 + outbeep1);
      break;
    case 0x20:		// Cassette tape audio
    case 0x40:		// I/O channel audio
      speakerout = 0;
      break;
    case 0x60:		// Sound generator output
      speakerout = soundtmp - 8192;
      break;
    }
  if (!filloffset)
    clkcount_at_buf0 = clkcount;
  localbuf[filloffset] = speakerin + speakerout;
  outbeep0 = 0;
  outbeep1 = 0;
  internalbeep0 = 0;
  internalbeep1 = 0;
  if (filloffset + 1 >= buffersize)
    filloffset = 0;
  else
    filloffset++;
  if (SDL_AtomicAdd (&fillsize, 1) + 1u == buffersize)
    SDL_SemWait (semaphore);
}

void
sdlsound::clk (int clockcount)
{
  // Use a simple fast loop for each counter until generating audio to
  // improve performance
  int n = (14318180 - 1 - clksum) / rate;
  if (n > clockcount)
    n = clockcount;
  int t = (timerclk + n) / 12;
  timerclk = (timerclk + n) % 12;
  for (int i = 0; i < t; i++)
    tick_pit ();
  t = (soundclk + n) / 4;
  soundclk = (soundclk + n) % 4;
  for (int i = 0; i < t; i++)
    tick_sound ();
  clksum += rate * n;
  clkcount += n;
  // Slow loop...
  for (int i = n; i < clockcount; i++)
    {
      // PIT CLK input: 14.31818MHz / 12 = 1.193182 MHz
      timerclk++;
      if (timerclk >= 12)
	{
	  timerclk -= 12;
	  tick_pit ();
	}
      // SN76489 CLK input: 14.31818MHz / 4 = 3.579545MHz
      soundclk++;
      if (soundclk >= 4)
	{
	  soundclk -= 4;
	  tick_sound ();
	}
      // Generating audio data: rate = 14.31818MHz * (rate / 14.31818MHz)
      clksum += rate;
      if (clksum >= 14318180)
	{
	  clksum -= 14318180;
	  tick_genaudio ();
	}
      clkcount++;
    }
}
