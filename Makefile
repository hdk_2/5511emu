#ARCH=-march=pentium-m
CXXFLAGS=-g -Wall -O2 $(ARCH) `pkg-config sdl2 --cflags`

.PHONY : all clean
all : 5511emu

clean :
	rm jmain.o jmem.o sdlsound.o jvideo.o jkey.o jfdc.o stdfdc.o 8088.o 8259a.o jjoy.o jbus.o jio1ff.o jrtc.o sdlvideo.o jparam.o jsound.o sdlmain.o

5511emu : jmain.o jmem.o sdlsound.o jvideo.o jkey.o jfdc.o stdfdc.o 8088.o 8259a.o jjoy.o jbus.o jio1ff.o jrtc.o sdlvideo.o jparam.o jsound.o sdlmain.o
	$(CXX) -g -o 5511emu jmain.o jmem.o sdlsound.o jvideo.o jkey.o jfdc.o stdfdc.o 8088.o 8259a.o jjoy.o jbus.o jio1ff.o jrtc.o sdlvideo.o jparam.o jsound.o sdlmain.o `pkg-config sdl2 --libs`

8088.o : 8088.cc 8088.hh 8259a.hh

8259a.o : 8259a.cc 8259a.hh 8088.hh

jmain.o : jmain.cc jtype.hh jmem.hh jvideo.hh jkey.hh jfdc.hh jsound.hh jjoy.hh jbus.hh jio1ff.hh jrtc.hh 8088.hh 8259a.hh jparam.hh jmain.hh

jmem.o : jmem.cc jmem.hh

sdlsound.o : sdlsound.cc jmem.hh jvideo.hh sdlsound.hh 8259a.hh jsound.hh

jvideo.o : jvideo.cc jvideo.hh jtype.hh jmem.hh 8259a.hh

jkey.o : jkey.cc jkey.hh jtype.hh

jfdc.o : jfdc.cc jfdc.hh jvideo.hh jtype.hh jmem.hh 8259a.hh

stdfdc.o : stdfdc.cc stdfdc.hh jfdc.o jtype.hh jmem.hh jvideo.hh

jjoy.o : jjoy.cc jjoy.hh

jbus.o : jbus.cc jbus.hh

jio1ff.o : jio1ff.cc jbus.hh jio1ff.hh

jrtc.o : jrtc.cc jrtc.hh

sdlvideo.o : sdlvideo.cc jmem.hh jvideo.hh sdlvideo.hh

jparam.o : jparam.cc jparam.hh

jsound.o : jsound.cc jsound.hh

sdlmain.o : sdlmain.cc jtype.hh jmem.hh jkey.hh 8088.hh jvideo.hh jsound.hh jfdc.hh jjoy.hh jparam.hh jmain.hh sdlvideo.hh sdlsound.hh stdfdc.hh
