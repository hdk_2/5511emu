/*
  Copyright (C) 2000-2016  Hideki EIRAKU

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

class sdlsound : public jsound
{
private:
  unsigned int rate, buffersize;
  signed short *localbuf;
  unsigned int clksum;
  unsigned int copyoffset;
  unsigned int filloffset;
  int samples;
  SDL_atomic_t fillsize;
  bool playing;
  unsigned int timerclk;
  unsigned int internalbeep0, internalbeep1;
  unsigned int outbeep0, outbeep1;
  unsigned int soundclk;
  SDL_sem *semaphore;
  SDL_TimerID timer_id;
  SDL_sem *closing;
  void tick_pit ();
  void tick_sound ();
  void tick_genaudio ();
  void audiocallback (Uint8 *stream, int len);
  static Uint32 sdltimercallback (Uint32 interval, void *param);
  static void sdlaudiocallback (void *data, Uint8 *stream, int len);
  jvideo::hw &videohw;
  unsigned int clkcount;
  unsigned int clkcount_at_buf0;
public:
  void clk (int clockcount);
  sdlsound (unsigned int rate, unsigned int buffersize, unsigned int samples,
	    jvideo::hw &videohw);
  ~sdlsound ();
};
