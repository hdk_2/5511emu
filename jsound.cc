/*
  Copyright (C) 2000-2016  Hideki EIRAKU

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include "jsound.hh"

////////////////////////////////////////////////////////////
// 8253

jsound::j8253::j8253counter::j8253counter ()
{
  // Just clear all variables
  counting = false;
  latching = false;
  setting = false;
  readmsb = false;
  writemsb = false;
  togglemsb = false;
  modebcd = 0;
  outlatch = 0;
  cntelement = 0;
  cntreload = 0;
  cntreg = 0;
  out = false;
  gate = false;
  trigger = false;
  strobe = false;
}

void
jsound::j8253::j8253counter::initout ()
{
  switch (modebcd & 0xe)
    {
    case 0x0:	  // Mode 0: Interrupt on terminal count
      out = false;
      break;
    case 0x2:	  // Mode 1: Hardware retriggerable one-shot
    case 0x4:	  // Mode 2: Rate generator
    case 0xc:	  // Mode 2
    case 0x6:	  // Mode 3: Square wave mode
    case 0xe:	  // Mode 3
    case 0x8:	  // Mode 4: Software triggered strobe
    case 0xa:	  // Mode 5: Hardware triggered strobe (retriggerable)
      out = true;
      break;
    }
}

void
jsound::j8253::j8253counter::write_control (unsigned int val)
{
  switch (val & 0x30)
    {
    case 0x00:			// Counter latch command
      if (!latching)		// Ignore if this is already latched
	{
	  outlatch = cntelement;
	  latching = true;
	}
      return;
    case 0x10:			// LSB only
      readmsb = false;
      writemsb = false;
      togglemsb = false;
      break;
    case 0x20:			// MSB only
      readmsb = true;
      writemsb = true;
      togglemsb = false;
      break;
    case 0x30:			// LSB then MSB
      readmsb = false;
      writemsb = false;
      togglemsb = true;
      break;
    }
  counting = false;
  setting = false;
  strobe = false;
  modebcd = val & 0xf;
  initout ();
}

unsigned int
jsound::j8253::j8253counter::read_counter ()
{
  unsigned int val;

  if (latching)
    val = outlatch;
  else
    val = cntelement;
  if ((modebcd & 0x6) == 0x6)	// Mode 3, (modebcd & 0xe) is 0x6 or 0xe
    val &= 0xfffe;		// Mode 3 counter must be even
  if (readmsb)
    val >>= 8;
  val &= 0xff;
  if (!togglemsb || readmsb)
    latching = false;
  if (togglemsb)
    readmsb = !readmsb;
  return val;
}

void
jsound::j8253::j8253counter::write_counter (unsigned int val)
{
  setting = false;
  val &= 0xff;
  if (writemsb)
    val <<= 8;
  if (!togglemsb || !writemsb)
    cntreg = 0;
  if ((modebcd & 0xe) == 0x0)	// Mode 0
    {
      counting = false;
      initout ();
    }
  cntreg |= val;
  if (!togglemsb || writemsb)
    setting = true;
  if (togglemsb)
    writemsb = !writemsb;
}

void
jsound::j8253::j8253counter::setgate (bool val)
{
  // GATE has no effect on OUT except mode 2 or mode 3
  if ((modebcd & 0x4) == 0x4)	// Mode 2 or mode 3
    {
      if (gate && !val)
	out = true;
    }
  if (!gate && val)
    trigger = true;
  gate = val;
}

void
jsound::j8253::j8253counter::reload ()
{
  if (setting)
    cntreload = cntreg;
  cntelement = cntreload;
  setting = false;
  counting = true;
}

unsigned int
jsound::j8253::j8253counter::decrement ()
{
  if ((modebcd & 0x1) == 0x1)	// BCD
    {
      if ((cntelement & 0xf) == 0x0)
	{
	  if ((cntelement & 0xf0) == 0x00)
	    {
	      if ((cntelement & 0xf00) == 0x000)
		{
		  if ((cntelement & 0xf000) == 0x0000)
		    {
		      // 0000 - 1 = 9999
		      cntelement = 0x9999;
		    }
		  else
		    {
		      // 1000 - 1 = 0999
		      cntelement -= 0x667;
		    }
		}
	      else
		{
		  // 0100 - 1 = 0099
		  cntelement -= 0x67;
		}
	    }
	  else
	    {
	      // 0010 - 1 = 0009
	      cntelement -= 0x7;
	    }
	}
      else
	{
	  // 0001 - 1 = 0000
	  cntelement--;
	}
    }
  else
    {
      if (cntelement == 0)
	cntelement = 0xffff;
      else
	cntelement--;
    }
  return cntelement;
}

void
jsound::j8253::j8253counter::tick ()
{
  switch (modebcd & 0xe)
    {
    case 0x0:			// Mode 0
      if (setting)
	{
	  reload ();
	  out = false;
	  break;		// this cycle does not decrement
	}
      if (!counting)
	break;
      if (!gate)
	break;
      if (decrement () == 0)
	out = true;
      break;
    case 0x2:			// Mode 1
      if (!counting && setting)
	{
	  reload ();
	  out = true;
	  break;
	}
      if (!counting)
	break;
      if (trigger)
	{
	  reload ();
	  out = false;
	}
      else
	{
	  if (decrement () == 0)
	    out = true;
	}
      break;
    case 0x4:			// Mode 2
    case 0xc:			// Mode 2
      if (!counting && setting)
	{
	  reload ();
	  out = true;
	  break;
	}
      if (!counting)
	break;
      if (!gate)
	break;
      if (trigger)
	{
	  reload ();
	  out = true;
	  break;
	}
      switch (decrement ())
	{
	case 1:
	  out = false;
	  break;
	case 0:
	  reload ();
	  out = true;
	  break;
	}
      break;
    case 0x6:			// Mode 3
    case 0xe:			// Mode 3
      if (!counting && setting)
	{
	  reload ();
	  out = true;
	  break;
	}
      if (!counting)
	break;
      if (!gate)
	break;
      if (trigger)
	{
	  reload ();
	  out = true;
	  break;
	}
      if (decrement () == 0)
	{
	  // Odd counts only: OUT is high here and will be low
	  // Decrementing after reload will make counter even counts
	  out = false;
	  reload ();
	  // if count is not odd here, we need to skip decrementing
	  // because counter is changed to even counts now.
	  if ((cntelement & 1) == 0)
	    break;
	}
      if (decrement () == 0)
	{
	  // Even counts: OUT is unknown
	  // Odd counts: OUT is low here and will be high
	  out = !out;
	  reload ();
	  // if count is odd and out is low, the counter is changed from
	  // even counts to odd counts.
	  if ((cntelement & 1) != 0 && out == false)
	    decrement ();
	}
      break;
    case 0x8:			// Mode 4
      if (setting)
	{
	  reload ();
	  out = true;
	  strobe = true;
	  break;		// this cycle does not decrement
	}
      if (!counting)
	break;
      if (!gate)
	break;
      if (decrement () == 0 && strobe)
	{
	  out = false;
	  strobe = false;
	}
      else
	{
	  out = true;
	}
      break;
    case 0xa:			// Mode 5
      if (trigger)
	{
	  reload ();
	  out = true;
	  strobe = true;
	  break;		// this cycle does not decrement
	}
      if (!counting)
	break;
      if (decrement () == 0 && strobe)
	{
	  out = false;
	  strobe = false;
	}
      else
	{
	  out = true;
	}
      break;
    }
  trigger = false;
};

////////////////////////////////////////////////////////////
// SN76489A

jsound::jsn76489a::jsn76489a ()
{
  int i;

  // Just clear all variables
  div32 = 0;
  saved_data = 0;
  noise_register = 0;
  noise_update = false;
  noise_white = false;
  noise_shift = 0;
  for (i = 0; i < 4; i++)
    {
      attenuator[i] = 0xf;	// Off
      frequency[i] = 0;
      counter[i] = 0;
    }
  for (i = 0; i < 3; i++)
    outbit[i] = false;
  outsum = 0;
  outcnt = 0;
}

void
jsound::jsn76489a::tick ()
{
  int i;
  unsigned int noise_frequency;

  if (div32 > 1)
    {
      div32--;
      return;
    }
  div32 = 32;
  // Just like 8253 mode 3 counter
  for (i = 0; i < 3; i++)
    {
      if (counter[i] == 0 && frequency[i] == 0)
	{
	  outbit[i] = false;
	  continue;
	}
      if (counter[i]-- <= 1)
	{
	  outbit[i] = false;
	  counter[i] = frequency[i];
	  if (counter[i] <= 22)	// low pass filter 4.8kHz
	    counter[i] = 0;
	  if ((counter[i] & 1) == 0)
	    continue;
	}
      if (counter[i]-- <= 1)
	{
	  outbit[i] = !outbit[i];
	  counter[i] = frequency[i];
	  if (counter[i] <= 22)	// low pass filter 4.8kHz
	    counter[i] = 0;
	  if ((counter[i] & 1) != 0 && outbit[i] == false)
	    counter[i]--;
	}
    }
  // Noise
  if (counter[3]-- <= 1)
    {
      switch (noise_register & 0x3)
	{
	default:		// Avoid compiler warning
	case 0x0:		// N / 512
	  noise_frequency = 16;
	  break;
	case 0x1:		// N / 1024
	  noise_frequency = 32;
	  break;
	case 0x2:		// N / 2048
	  noise_frequency = 64;
	  break;
	case 0x3:		// Tone 3 frequency
	  noise_frequency = frequency[2] * 2;
	  if (noise_frequency <= 22) // low pass filter 4.8kHz
	    noise_frequency = 0;
	  break;
	}
      if (noise_update)
	{
	  if ((noise_register & 0x4) != 0)
	    {
	      // White noise
	      noise_shift = 0x4001;
	      noise_white = true;
	    }
	  else
	    {
	      // Periodic noise
	      noise_shift = 1;
	      noise_white = false;
	    }
	  noise_update = false;
	}
      counter[3] = noise_frequency;
      if (noise_frequency != 0)
	{
	  outbit[3] = ((noise_shift & 0x1) != 0) ? true : false;
	  if (noise_white)
	    {
	      if (((noise_shift ^ noise_shift >> 1) & 0x1) != 0)
		noise_shift = 0x4000 | noise_shift >> 1;
	      else
		noise_shift >>= 1;
	    }
	  else
	    {
	      if ((noise_shift & 0x1) != 0)
		noise_shift = 0x2000;
	      else
		noise_shift >>= 1;
	    }
	}
      else
	{
	  outbit[3] = false;
	}
    }
  // Calculate output
  for (i = 0; i < 4; i++)
    {
      if (outbit[i])
	outsum += 546 * (0xf - attenuator[i]);
    }
  outcnt++;
}

unsigned int
jsound::jsn76489a::getdata ()
{
  unsigned int r;

  r = outsum / outcnt;
  outsum = 0;
  outcnt = 0;
  if (r > 32767)
    return 32767;
  else
    return r;
}

void
jsound::jsn76489a::outb (unsigned int data)
{
  switch (data & 0xf0)
    {
    case 0x80:			// Tone 1 frequency
    case 0xa0:			// Tone 2 frequency
    case 0xc0:			// Tone 3 frequency
      frequency[(data >> 5) & 3] &= ~0xf;
      frequency[(data >> 5) & 3] |= data & 0xf;
      saved_data = data;	// These are two bytes command.
      break;
    case 0xe0:			// Noise
      noise_register = data;
      noise_update = true;
      break;
    case 0x90:			// Tone 1 attenuator
    case 0xb0:			// Tone 2 attenuator
    case 0xd0:			// Tone 3 attenuator
    case 0xf0:			// Noise attenuator
      attenuator[(data >> 5) & 3] = data & 0xf;
      break;
    default:			// Two bytes command
      frequency[(saved_data >> 5) & 3] = ((data & 0x3f) << 4)
	| (saved_data & 0xf);
      break;
    }
}
