/*
  Copyright (C) 2000-2016  Hideki EIRAKU

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

class jsound
{
protected:
  // 8253-5 Programmable Interval Timer
  class j8253
  {
  private:
    class j8253counter
    {
    private:
      unsigned int cntreg;
      unsigned int cntelement;
      unsigned int cntreload;
      unsigned int outlatch;
      unsigned int modebcd;
      bool counting, latching, setting;
      bool readmsb, writemsb;
      bool togglemsb;
      bool out, gate;
      bool trigger, strobe;
      void initout ();
      void reload ();
      unsigned int decrement ();
    public:
      j8253counter ();
      void write_control (unsigned int val);
      unsigned int read_counter ();
      void write_counter (unsigned int val);
      void setgate (bool val);
      bool getout ();
      void tick ();
    };
    j8253counter counter[4];
  public:
    void out8253 (unsigned int addr, unsigned int val);
    unsigned int in8253 (unsigned int addr);
    void setgate (unsigned int addr, bool val);
    bool getout (unsigned int addr);
    void tick (unsigned int addr);
  };
  // SN76489A Digital Complex Sound Generator
  class jsn76489a
  {
  private:
    unsigned int div32;
    unsigned int saved_data;
    unsigned int noise_register;
    unsigned int noise_shift;
    unsigned int attenuator[4];
    unsigned int frequency[4];
    unsigned int counter[4];
    bool noise_update, noise_white;
    bool outbit[4];
    unsigned int outsum, outcnt;
  public:
    jsn76489a ();
    void tick ();
    unsigned int getdata ();
    void outb (unsigned int data);
  };
  j8253 pit;
  jsn76489a sn76489a;
  unsigned int pb;
  bool timer1sel;
public:
  // Sound common
  virtual void clk (int clockcount) = 0;
  void iowrite (unsigned char data);
  void out8253 (unsigned int addr, unsigned int val);
  unsigned int in8253 (unsigned int addr);
  bool gettimer2out ();
  void set8255b (unsigned int val);
  void selecttimer1in (bool timer0out);
};

inline bool
jsound::j8253::j8253counter::getout ()
{
  return out;
}

inline void
jsound::j8253::out8253 (unsigned int addr, unsigned int val)
{
  addr &= 3;
  if (addr <= 2)
    counter[addr].write_counter (val);
  else
    counter[(val & 0xc0) >> 6].write_control (val);
}

inline unsigned int
jsound::j8253::in8253 (unsigned int addr)
{
  addr &= 3;
  if (addr <= 2)
    return counter[addr].read_counter ();
  else
    return 0xff;
}

inline void
jsound::j8253::setgate (unsigned int addr, bool val)
{
  counter[addr].setgate (val);
}

inline bool
jsound::j8253::getout (unsigned int addr)
{
  return counter[addr].getout ();
}

inline void
jsound::j8253::tick (unsigned int addr)
{
  counter[addr].tick ();
}

inline void
jsound::iowrite (unsigned char data)
{
  sn76489a.outb (data);
}

inline void
jsound::out8253 (unsigned int addr, unsigned int val)
{
  pit.out8253 (addr, val);
}

inline unsigned int
jsound::in8253 (unsigned int addr)
{
  return pit.in8253 (addr);
}

inline bool
jsound::gettimer2out ()
{
  return pit.getout (2);
}

inline void
jsound::set8255b (unsigned int val)
{
  pit.setgate (2, ((val & 0x1) != 0) ? true : false);
  pb = val;
}

inline void
jsound::selecttimer1in (bool timer0out)
{
  timer1sel = timer0out;
}
