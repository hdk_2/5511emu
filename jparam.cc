/*
  Copyright (C) 2000-2016  Hideki EIRAKU

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#include <iostream>
#include <cstring>
#include "jparam.hh"

jparam
parse_argv (int argc, char **argv)
{
  jparam ret;
  ret.ok = false;
  ret.exsize = false;
  ret.novsync = false;
  ret.pcjrflag = false;
  ret.warmflag = false;
  ret.origpcjrflag = false;
  ret.fastflag = false;
  ret.seekcheck = false;
  for (int i = 0; i < 6; i++)
    ret.cart[i] = NULL;
  for (int i = 0; i < 4; i++)
    ret.fdfile[i] = NULL;
  char *memsize = NULL;
  for (int i = 1, j = 0; i < argc; i++)
    {
      if (strcmp (argv[i], "-j") == 0)
	ret.pcjrflag = true;
      else if (strcmp (argv[i], "-w") == 0)
	ret.warmflag = true;
      else if (strcmp (argv[i], "-o") == 0)
	ret.origpcjrflag = true;
      else if (strcmp (argv[i], "-e") == 0)
	ret.exsize = true;
      else if (strcmp (argv[i], "-f") == 0)
	ret.fastflag = true;
      else if (strcmp (argv[i], "-s") == 0)
	ret.novsync = true;
      else if (strcmp (argv[i], "-m") == 0 && i + 1 < argc)
	memsize = argv[++i];
      else if (strcmp (argv[i], "-seekcheck") == 0)
	ret.seekcheck = true;
      else if (strcmp (argv[i], "-d0") == 0 && i + 1 < argc)
	ret.cart[0] = argv[++i];
      else if (strcmp (argv[i], "-d8") == 0 && i + 1 < argc)
	ret.cart[1] = argv[++i];
      else if (strcmp (argv[i], "-e0") == 0 && i + 1 < argc)
	ret.cart[2] = argv[++i];
      else if (strcmp (argv[i], "-e8") == 0 && i + 1 < argc)
	ret.cart[3] = argv[++i];
      else if (strcmp (argv[i], "-f0") == 0 && i + 1 < argc)
	ret.cart[4] = argv[++i];
      else if (strcmp (argv[i], "-f8") == 0 && i + 1 < argc)
	ret.cart[5] = argv[++i];
      else if (j < 4)
	ret.fdfile[j++] = argv[i];
    }
  ret.ok = true;
  if (memsize)
    {
      if (!strcmp (memsize, "64"))
	ret.memsize = 64;
      else if (!strcmp (memsize, "128"))
	ret.memsize = 128;
      else if (!strcmp (memsize, "256"))
	ret.memsize = 256;
      else if (!strcmp (memsize, "384"))
	ret.memsize = 384;
      else if (!strcmp (memsize, "512"))
	ret.memsize = 512;
      else if (!strcmp (memsize, "640") && ret.origpcjrflag)
	ret.memsize = 640;
      else
	{
	  std::cerr << "Invalid memory size: " << memsize << std::endl;
	  ret.memsize = 0;
	  ret.ok = false;
	}
    }
  else
    {
      if (ret.origpcjrflag)
	ret.memsize = 640;
      else
	ret.memsize = 512;
    }
  return ret;
}
