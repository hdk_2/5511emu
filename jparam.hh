/*
  Copyright (C) 2000-2016  Hideki EIRAKU

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

struct jparam
{
  bool ok;
  bool exsize;
  bool novsync;
  bool pcjrflag;
  bool warmflag;
  bool origpcjrflag;
  bool fastflag;
  bool seekcheck;
  char *cart[6];		// D0, D8, E0, E8, F0, F8
  char *fdfile[4];
  int memsize;
};

jparam parse_argv (int argc, char **argv);
